import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DocumentCollection, Service } from 'ngx-jsonapi';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map, skipWhile, tap } from 'rxjs/operators';
import { environment } from '../environment';
import { PointOfSale } from '../_models/point-of-sale';
import { Supplier } from '../_models/supplier';

@Injectable({
  providedIn: 'root'
})
export class SuppliersService extends Service<Supplier> {
  public resource = Supplier;
  public type = 'suppliers';
  supplier$ = new BehaviorSubject<Supplier>(null);

  private url = `${environment.miamAPI}/api/v1`;

  constructor(private http: HttpClient) {
    super();
    this.register();
  }

  loadSupplier(supplierId: string): Observable<Supplier> {
    if (!this.supplier$.value || this.supplier$.value.id !== supplierId) {
      this.get(supplierId).pipe(
        skipWhile(supplier => supplier.is_loading),
        tap(supplier => this.supplier$.next(supplier))
      ).subscribe();
    }

    return this.supplier$;
  }

  // TODO: api : add related relationship route
  getSupplierPointOfSales(supplierId: string): Observable<PointOfSale[]> {
    return this.http.get<DocumentCollection<PointOfSale>>(`${this.url}/point-of-sales?filter[supplier_id]=${supplierId}`).pipe(
      map((res: DocumentCollection<PointOfSale>) => res.data));
  }

  notifyBasketUpdated(basketToken: string, status: string, price: number | string = null): Observable<any> {
    if (!this.supplier$.value) {
      return of(null);
    }

    const notificationUrl = `${this.url}/suppliers/${this.supplier$.value.id}/webhooks/basket_updated`;
    const body = { token: basketToken, status: status };
    if (price) {
      body['total_price'] = price;
    }
    return this.http.post(notificationUrl, { body: body });
  }

}

import { ElementRef, Injectable } from '@angular/core';

/**
 * this service is made to add class miam-not-printable to root DOM's elements
 * this class made then not visible while printing , then add element pass in param 
 * in DOM it will be the unique element visible while printing, after closing printing modal
 * it will remove as well the class and the element add in DOM
 *
 * Best practice 'll be to create a blob in backend and then open it on an other tab.
 * but this solution wont help us to diplay properly recipe if user press ctrl+p
 * not that this solution can interfer with print style of host web site an may cause
 * unexpected print issue
 */
@Injectable({
  providedIn: 'root'
})
export class PrintService {

  private printContainer: HTMLDivElement;
  private body: HTMLBodyElement;

  constructor() {
    this.body = document.getElementsByTagName('body')[0];
  }

  prepareForPrint(elementToPrint: ElementRef) {

    if (!elementToPrint) return
    // add not printable class to each DOM root element
    for (let i = 0; i < this.body.children.length; i++) {
      this.body.children[i].classList.add('miam-not-printable');
    }
    const printContainer = document.createElement('div');
    printContainer.classList.add('miam-print-only');

    window.onbeforeprint = ()  => {
      this.printContainer.appendChild(document.createRange().createContextualFragment(elementToPrint.nativeElement.innerHTML));
    };
    this.body.appendChild(printContainer);
    // add elemnt to print to DOM it 'll be the only element to show in print mode
    this.printContainer = printContainer;
  }

  dissmissPrinting() {
    // remove element to print from DOM
   this.printContainer.remove();

    // remove not printable class of DOM element to not interact with other print fonctionality on web site
    for (let i = 0; i < this.body.children.length; i++) {
      this.body.children[i].classList.remove('miam-not-printable');
    }
  }
}

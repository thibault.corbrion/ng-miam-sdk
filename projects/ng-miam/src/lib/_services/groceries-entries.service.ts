import { Injectable } from '@angular/core';
import { Service } from 'ngx-jsonapi';
import { GroceriesEntry } from '../_models/groceries-entry';

@Injectable({
  providedIn: 'root'
})
export class GroceriesEntriesService extends Service<GroceriesEntry> {
  public resource = GroceriesEntry;
  public type = 'groceries-entries';

  constructor() {
    super();
    this.register();
  }
}

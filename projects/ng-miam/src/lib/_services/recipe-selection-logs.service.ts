import { Injectable } from '@angular/core';
import { RecipeSelectionLog } from '../_models/recipe-selection-log';
import { Service } from 'ngx-jsonapi';

@Injectable({
  providedIn: 'root',
})
export class RecipeSelectionLogsService extends Service<RecipeSelectionLog> {
  resource = RecipeSelectionLog;
  type = 'recipe-selection-logs';

  constructor() {
    super();
    this.register();
  }
}

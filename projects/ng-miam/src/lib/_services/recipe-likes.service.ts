import { Injectable } from '@angular/core';
import { Service } from 'ngx-jsonapi';
import { RecipeLike } from '../_models/recipe-like';

@Injectable({
  providedIn: 'root'
})
export class RecipeLikesService extends Service<RecipeLike> {

  public resource = RecipeLike;
  public type = 'recipe-likes';

  constructor() {
    super();
    this.register();
  }
}

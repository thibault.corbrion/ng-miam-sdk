import { Injectable } from "@angular/core";
import { Service } from "ngx-jsonapi";
import { Sponsor } from "../_models/sponsor";

@Injectable({
  providedIn: 'root'
})
export class SponsorService extends Service<Sponsor> {

  public resource = Sponsor;
  public type = 'sponsors';

  constructor() {
    super();
    this.register();
  }
}

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { NgxJsonapiModule, Service } from 'ngx-jsonapi';
import { GroceriesListsService } from './groceries-lists.service';
import { environment } from '../environment';
import { GroceriesList } from '../_models/groceries-list';
import { MIAM_BASKET_PATH } from '../providers';
import { of } from 'rxjs';

const MIAM_WEB_URL = `${environment.miamWeb}/fr/app`;
const MIAM_BASKET_URL = `${MIAM_WEB_URL}/mon-panier`;

describe('GroceriesListsService', () => {
  let httpTestingController: HttpTestingController;
  let listsService: GroceriesListsService;

  beforeEach(() => {
    spyOn(GroceriesList.prototype, 'save').and.callFake(() => of('new list!'));
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        NgxJsonapiModule.forRoot({
          url: [environment.miamAPI, 'api/v1/'].join('/'),
        })
      ],
      providers: [
        GroceriesListsService,
        [{ provide: MIAM_BASKET_PATH, useValue: MIAM_BASKET_URL }]
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
    listsService = TestBed.inject(GroceriesListsService);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be a NgxJsonApi Service', () => {
    expect(listsService instanceof Service).toBe(true);
    expect(listsService.resource).toEqual(GroceriesList);
    expect(listsService.type).toEqual('groceries-lists');
  });

  // describe('postNew(entries)', () => {
  //   let ingredients: GroceriesEntry[] = [
  //     new GroceriesEntry('pommes de terre', '1', 'Kg'),
  //     new GroceriesEntry('huile d\'olive', '5', 'cL'),
  //     new GroceriesEntry('sel'),
  //   ];
  //   let saveSpy;
  //   let saveCall;
  //   function subject() {
  //     listsService.postNew(ingredients).subscribe(() => {
  //       saveCall = saveSpy.calls.first();
  //     });
  //   };

  //   beforeEach(() => {
  //     saveSpy = spyOn(GroceriesList.prototype, 'save').and.callFake(() => of('new list!'));
  //   });

  //   it('posts a list', () => {
  //     subject();
  //     expect(saveSpy).toHaveBeenCalled();
  //     expect(saveCall.object).toEqual(jasmine.any(GroceriesList));
  //   })

  //   it('removes id and groceries-entries from the list before posting', () => {
  //     subject();
  //     expect(saveCall.object.id).not.toBeDefined();
  //     expect(saveCall.object.relationships['groceries-entries']).not.toBeDefined();
  //   });

  //   it('adds entries to the list before posting', () => {
  //     subject();
  //     expect(saveCall.object.attributes.entries).toEqual([
  //       jasmine.objectContaining({ name: 'pommes de terre', 'capacity-volume': '1', 'capacity-unit': 'Kg', 'capacity-factor': '1' }),
  //       jasmine.objectContaining({ name: 'huile d\'olive', 'capacity-volume': '5', 'capacity-unit': 'cL', 'capacity-factor': '1' }),
  //       jasmine.objectContaining({ name: 'sel', 'capacity-volume': '1', 'capacity-unit': '', 'capacity-factor': '1' })
  //     ]);
  //   });

  //   describe('when an entry does not have any name', () => {
  //     beforeEach(() => {
  //       delete(ingredients[1].attributes.name);
  //     });

  //     it('does not post the entry with the list', () => {
  //       subject();
  //       expect(saveCall.object.attributes.entries).toEqual([
  //         jasmine.objectContaining({ name: 'pommes de terre', 'capacity-volume': '1', 'capacity-unit': 'Kg', 'capacity-factor': '1' }),
  //         jasmine.objectContaining({ name: 'sel', 'capacity-volume': '1', 'capacity-unit': '', 'capacity-factor': '1' })
  //       ]);
  //     });
  //   })
  // });
});

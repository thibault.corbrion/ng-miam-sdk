import {Injectable} from '@angular/core';
import {Service} from 'ngx-jsonapi';
import {RecipeType} from '../_models/recipe-type';
import {tap} from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RecipeTypeService extends Service<RecipeType> {

  public resource = RecipeType;
  public type = 'recipe-types';
  private types: BehaviorSubject<RecipeType[]>;


  constructor() {
    super();
    this.register();
    this.types = new BehaviorSubject(null)
    this.init();
  }

  init() {
     this.all()
      .pipe(
        tap(types => this.types.next(types.data) )
      ).toPromise();
  }

  getTypes(): Observable<RecipeType[]> {

    return this.types.asObservable();
  }

  getType(index: number) {
    return this.types[index];
  }

  getTypeByName(name: string) {
    name = name.toLowerCase().trim();
    return this.types.value.filter(t => t.attributes.name.toLowerCase().trim() === name)[0];
  }

}

import {Injectable} from '@angular/core';
import { environment } from '../environment';
import { Recipe } from '../_models';
import { HttpClient } from '@angular/common/http';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RecipeEventsService  {
  suggestion_url = `${environment.miamAPI}/api/events/recipes`;
  public ORIGIN_SUGGESTION = 'suggestion'
  public ACTION_PROPOSED = 'proposed'
  public ACTION_SHOWN = 'shown'
  public ACTION_DISPLAYED = 'displayed'
  public ACTION_ADDED = 'added'
  public ACTION_PUSHED = 'pushed'

  constructor(private http: HttpClient) { }

  sendEvent(recipe: Recipe, action: string){
    if(!recipe.eventOrigin){
      return
    }
    let url = `${this.suggestion_url}/${recipe.id}/${recipe.eventOrigin}/${action}`;
    if (recipe.eventsGroupId){
      url += `?group_id=${recipe.eventsGroupId}`
    }
    this.http.post(url, {}).subscribe()
  }
}

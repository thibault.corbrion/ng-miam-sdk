import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DocumentCollection, Service } from 'ngx-jsonapi';
import { forkJoin, Observable, of } from 'rxjs';
import { map, skipWhile, switchMap } from 'rxjs/operators';
import { environment } from '../environment';
import { PointOfSale } from '../_models/point-of-sale';
import { Recipe } from '../_models/recipe';
import { RecipeProvider } from '../_models/recipe-provider';
import { RecipeShare } from '../_models/recipe-share';
import { Supplier } from '../_models/supplier';

const MIAM_API_HOST = `${environment.miamAPI}/api/v1/`;

@Injectable({
  providedIn: 'root',
})

export class RecipeShareService extends Service<RecipeShare> {
  resource = RecipeShare;
  type = 'recipe-shares';

  constructor(private http: HttpClient) {
    super();
    this.register();
  }

  // Each recipe share has a unique virtual id which is a combination of its destination and shareable
  // example of a virtual_id : suppliers:3-recipes:2520
  virtualId(destination: PointOfSale | Supplier, shareable: RecipeProvider | Recipe): string {
    return `${destination.type.replace(/-/g, '_')}:${destination.id}-${shareable.type.replace(/-/g, '_')}:${shareable.id}`;
  }

  // Fetch a recipe share based on its destination and shareable, using its virtual id
  getVirtual(destination: PointOfSale | Supplier, shareable: RecipeProvider | Recipe): Observable<RecipeShare> {
    if (!destination || !shareable) {
      return of(null);
    }

    const id = this.virtualId(destination, shareable);
    return this.get(id);
  }

  // Create or update a recipe share based on its destination and shareable, using its virtual id
  upsertVirtual(destination: Supplier | PointOfSale, shareable: RecipeProvider | Recipe, attrs: object): Observable<RecipeShare> {
    if (!destination || !shareable) {
      return of(null);
    }

    const existingShare: RecipeShare = this.new();
    Object.assign(existingShare, {
      id: this.virtualId(destination, shareable),
      is_new: false,
      attributes: attrs
    });
    return existingShare.save().pipe(map(() => existingShare));
  }

  getAllRecipeShareOfShareable(shareable: RecipeProvider | Recipe): Observable<DocumentCollection<RecipeShare>> {
    return this.all({
      remotefilter:
      {
        shareable_id: shareable.id,
        shareable_type: shareable.type
      }
    });
  }

  hasAtLeastOneRecipeShare(shareable: RecipeProvider | Recipe): Observable<boolean> {
    return this.getAllRecipeShareOfShareable(shareable).pipe(skipWhile(res => res.is_loading), switchMap(res => {
      return of(res.data.length > 0);
    }));
  }

  deleteAllRecipeShareOfShareable(shareable: RecipeProvider | Recipe): Observable<void[]> {
    return this.getAllRecipeShareOfShareable(shareable).pipe(skipWhile(res => res.is_loading), switchMap(recipeShares => {
      return forkJoin(recipeShares.data.map(recipeShare => {
       return this.delete(recipeShare.id);
      }));
    }));
  }

  // For shareable and destination, if only ids are passed, back will update shares for all elements with those ids, if only
  // filters are passed, back will use the filter to get the ids to update shares, and if both are passed, back will use the
  // filters to get ids, will remove those wich are in passed ids, and then update.
  updateMultipleRecipeShare(shareableIds: string[], shareableType: string, destinationIds: string[], destinationType: string,
                            status: string, shareableFilters: any, destinationFilters: any): Observable<any> {
    const body = {
      destination_ids: destinationIds,
      destination_type: destinationType,
      shareable_ids: shareableIds,
      shareable_type: shareableType,
      status,
      destination_filters: destinationFilters,
      shareable_filters: shareableFilters };
    const url = MIAM_API_HOST + `recipe-shares/multi`;
    return this.http.post(url, body);
  }
}

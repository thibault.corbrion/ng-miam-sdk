import { Injectable } from '@angular/core';
import { GroceriesListsService } from './groceries-lists.service';
import { PointOfSalesService } from './point-of-sales.service';
import { BasketsService } from './baskets.service';
import { RecipeProviderService } from './recipe-provider.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { AnalyticsService } from './analytics.service';
import { UserService } from './user.service';
import { Icon } from '../_types/icon.enum';
import { RecipesService } from './recipes.service';
import { MediaMatcher, BreakpointObserver } from '@angular/cdk/layout';
import { SuppliersService } from './suppliers.service';
import { User } from '../_models/user';

const SMALL_SCREEN_BP = '(min-width: 1023px)';

interface IconOverride {
  icon: Icon;
  url: string;
}

// Context shared between Miam SDK & host app
@Injectable({
  providedIn: 'root'
})
export class ContextService {
  userInfo: User;
  icons: IconOverride[];
  invalidPosCallback: () => any;

  private CORSIssues: BehaviorSubject<boolean>;
  private isSmallScreen$ = new BehaviorSubject(false);

  miam = {
    user: {
      setToken: (token, forbidProfiling = false) => {
        localStorage.setItem('_miam/userToken', token);
        localStorage.removeItem('_miam/userId');
        this.listsService.refreshCurrentList();
        return this.userService.setUserInfo(forbidProfiling);
      },
      loadWithExtId: (id, forbidProfiling = false) => {
        localStorage.setItem('_miam/userId', id);
        localStorage.removeItem('_miam/userToken');
        this.listsService.refreshCurrentList();
        return this.userService.setUserInfo(forbidProfiling);
      },
      reset: () => {
        localStorage.removeItem('_miam/userToken');
        localStorage.removeItem('_miam/userId');
        console.log('[Miam] will refresh list after reset')
        this.listsService.refreshCurrentList();
        this.userService.resetUserInfo();
      },
      device: this.mediaMatcher,
      isSmallScreen$: this.isSmallScreen$.asObservable()
    },
    list: {
      reset: () => this.listsService.resetList()
    },
    pos: {
      load: (posId) => {
        this.posService.loadPos(posId);
      },
      loadWithExtId: (extId, supplierId) => {
        this.posService.loadPosWithExtId(extId, supplierId);
      },
      pos$: this.posService.pos$
    },
    supplier: {
      load: (supplierId: number | string) => {
        this.suppliersService.loadSupplier(supplierId.toString());
      },
      notifyBasketUpdated: (basketToken: string, status: string, price: number | string = null) => {
        this.suppliersService.notifyBasketUpdated(basketToken, status, price).subscribe();
      }
    },
    basket: {
      stats$: this.basketsService.basketStats$,
      entries$: this.basketsService.basketEntries(),
      confirm: () => this.basketsService.confirmBasket(),
      paid: (price = 0) => {
        console.log('[Miam] send payment analytics event');
        this.analyticsService.sendEvent('pay-basket', null, price);
      }
    },
    recipes: {
      provider: {
        load: (id) => this.recipesProvidersService.loadCachedProvider(id)
      },
      display: (id) => this.recipesService.display(id),
      hidden: this.recipesService.hidden
    },
    analytics: {
      init: (key: string) => this.analyticsService.init(key)
    },

    /**
     * Override two times 'll not be considered
     * @param icon have to be a knowed icon name see section 'Custom icons' in read me
     * @param url of the new icon
     */
    overrideIcon: (icon, url) => {
      if (!this.icons) { this.icons = []; }
      this.icons.push({ icon, url });
    },

    setInvalidPosCallback: (callback: () => any) => {
      this.invalidPosCallback = callback;
    }
  }

  constructor(
    private posService: PointOfSalesService,
    private listsService: GroceriesListsService,
    private basketsService: BasketsService,
    private userService: UserService,
    private recipesProvidersService: RecipeProviderService,
    private analyticsService: AnalyticsService,
    private recipesService: RecipesService,
    private mediaMatcher: MediaMatcher,
    private suppliersService: SuppliersService,
    private breakpointObserver: BreakpointObserver
  ) {
    this.isSmallScreen$.next(this.mediaMatcher.matchMedia(SMALL_SCREEN_BP).matches);
    this.breakpointObserver.observe(SMALL_SCREEN_BP).subscribe(BPChange => this.isSmallScreen$.next(BPChange.matches));
    this.icons = [];
    this.CORSIssues = new BehaviorSubject(false);
    Object.assign(window, { miam: this.miam });

  }

  setCORSIssueState(value: boolean): void {
    this.CORSIssues.next(value);
  }

  getCORSIssuesState(): Observable<boolean> {
    return this.CORSIssues.asObservable();
  }

  getOverrideIconUrl(icon: Icon): string {
    const findicon = this.icons.find(el => el.icon.toString() === Icon[icon]);
    return findicon ? findicon.url : '';
  }
}

// THIS IS JUST AN EXAMPLE - UNUSED IN THE CODE

import { Injectable, ApplicationRef } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root' 
})
export class CounterService {

    counterNumber : BehaviorSubject<number>

    constructor(private app: ApplicationRef){
        this.counterNumber = new BehaviorSubject(1);

        this.counterNumber.asObservable().subscribe(() =>  this.app.tick())
    }


}
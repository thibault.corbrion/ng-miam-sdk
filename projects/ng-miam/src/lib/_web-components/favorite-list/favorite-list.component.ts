import { Component, ViewEncapsulation, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { DocumentCollection } from 'ngx-jsonapi';
import { BehaviorSubject } from 'rxjs';
import { skipWhile } from 'rxjs/operators';
import { Recipe } from '../../_models/recipe';
import { RecipesService } from '../../_services';
import { RecipeLikesService } from '../../_services/recipe-likes.service';

@Component({
  selector: 'ng-miam-favorite-recipe-list',
  templateUrl: './favorite-list.component.html',
  styleUrls: ['./favorite-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FavoriteListComponent {

  currentLikedRecipes$: BehaviorSubject<Array<Recipe>>;
  private lastPage = 0;

  constructor(
    private recipesService: RecipesService,
    private recipeLikesService: RecipeLikesService,
    private cdr: ChangeDetectorRef
  ) {
    this.currentLikedRecipes$ = new BehaviorSubject<Array<Recipe>>([]);
    this.initLikedRecipes();
  }

  initLikedRecipes() {
    this.loadMore();
  }

  loadMore() {
    this.lastPage++;
    this.loadRecipesPage(this.lastPage);
  }

  loadRecipesPage(page) {
    this.recipesService.all({
      page: { size: 20, number: page },
      remotefilter: { liked: true },
      include: ['ingredients', 'recipe-steps', 'recipe-status', 'sponsors']
    }).pipe(
      skipWhile(resp => resp.is_loading)
    )
    .subscribe((result: DocumentCollection<Recipe>) => {
      this.currentLikedRecipes$.next([...this.currentLikedRecipes$.getValue(), ...result.data]);
      this.cdr.detectChanges();
    });
  }

  removeFromFavorite(recipe: Recipe) {
    const liked = this.currentLikedRecipes$.getValue();
    const index = liked.findIndex(r => r.id === recipe.id);
    if(index != -1) {
      liked.splice(index, 1);
      this.currentLikedRecipes$.next([...liked])
      this.cdr.detectChanges();
    }
  }
}

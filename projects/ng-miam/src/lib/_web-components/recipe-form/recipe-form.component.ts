import {
  Component,
  ViewEncapsulation,
  ChangeDetectionStrategy,
  OnChanges,
  SimpleChanges,
  Input,
  Output,
  EventEmitter,
  ChangeDetectorRef,
  OnDestroy,
} from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Recipe } from '../../_models/recipe';
import * as _ from 'lodash';
import { Icon } from '../../_types/icon.enum';
import { OnInit } from '@angular/core';
import { forkJoin, Observable, of, Subscription } from 'rxjs';
import { Ingredient } from '../../_models/ingredient';
import { RecipeStep } from '../../_models/recipe-step';
import {
  IngredientsService,
  RecipesService,
  RecipeStatusService,
  RecipeStepsService,
  RecipeTypeService,
  UserService
} from '../../_services';
import { finalize, map, switchMap, take, tap } from 'rxjs/operators';
import { DocumentResource } from 'ngx-jsonapi';
import { RecipeProviderService } from '../../_services';
import { RecipeStatus } from '../../_models';


@Component({
  selector: 'ng-miam-recipe-form',
  templateUrl: './recipe-form.component.html',
  styleUrls: ['./recipe-form.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RecipeFormComponent implements OnChanges, OnInit, OnDestroy {

  @Input() recipe: Recipe;
  @Output() recipeChange: EventEmitter<Recipe>;
  @Output() canceled: EventEmitter<void>;

  public recipeForm: FormGroup;
  public isUpdate: boolean;
  public icon = Icon;
  public dificulties = ['facile', 'moyen', 'difficile'];
  public iconsDificulties = [this.icon.DifficultyLow, this.icon.DifficultyMedium, this.icon.DifficultyHight];
  public costs = ['faible', 'moyen', 'élevé'];
  public iconsCosts = [this.icon.CostLow, this.icon.CostMedium, this.icon.CostHight];
  public deleteIngredientsList: string[];
  public deleteStepsList: string[];
  public isAdmin: boolean;
  public statuses: Observable<RecipeStatus[]>;

  private formSubscription: Subscription;
  private subscriptions: Subscription[];

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private cdr: ChangeDetectorRef,
    public recipeProviderservice: RecipeProviderService,
    private recipeService: RecipesService,
    public recipeTypeService: RecipeTypeService,
    private ingredientService: IngredientsService,
    private recipeStepsService: RecipeStepsService,
    public statusService: RecipeStatusService
  ) {

    this.canceled = new EventEmitter();
    this.recipeChange = new EventEmitter();
    this.deleteIngredientsList = [];
    this.deleteStepsList = [];
    this.subscriptions = [];
    this.statuses = this.statusService.all().pipe(map(res => res.data));
  }

  ngOnInit(): void {
    this.subscriptions.push(
      this.userService.isAdmin.subscribe(isAdmin => {
        this.isAdmin = isAdmin;
        this.cdr.detectChanges();
      }));
    this.InitRecipe().pipe(take(1)).subscribe();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (_.has(changes, [`recipe`]) && this.recipe != null) {
      this.isUpdate = true;
      this.initForm(this.recipe);
    }
    this.cdr.detectChanges();
  }

  ngOnDestroy(): void {
    this.formSubscription.unsubscribe();
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }


  /**
   * Only for recipe creation.
   * Init blank recipe
   */
  private InitRecipe(): Observable<any> {
    // first state after creation 'll be SUBMITTED we preset this field because
    // normal user can't fill it and it's a mandatory field
    // admin can still override it
    if (this.recipe) return of(null);
    return this.statusService.submitted.pipe(switchMap(status =>
      this.userService.userProvider.pipe(tap(recipeProvider => {
        this.recipe = this.recipeService.new();
        this.recipe.addRelationship(status, 'recipe-status');
        this.recipe.addRelationship(recipeProvider, 'recipe-provider');
        delete this.recipe.id;
        this.initForm(this.recipe);
        this.cdr.detectChanges();
      }))));
  }


  public cancel() {
    this.canceled.emit();
  }

  public addToDeleteIngredientsList(ingredientId: string): void {
    this.deleteIngredientsList.push(ingredientId);
  }

  public addToDeleteStepsList(stepId: string): void {
    this.deleteStepsList.push(stepId);
  }

  public submit(): void {
    this.upsertRecipe().pipe(take(1)).subscribe(() => {
      this.deleteStepsList = [];
      this.deleteIngredientsList = [];
    });
  }

  /**
   * this function fill form values with given recipe
   * Form is divide in 4 part attributes,type, ingredients, steps
   * to folow recipe model in on hand and in other hand to update only
   * parts that have been change by user thanks to dirty attribute on
   * each of those parts
   * @param recipe use to fill form
   */
  private initForm(recipe: Recipe): void {
    if (this.formSubscription) {
      this.formSubscription.unsubscribe();
    }
    this.recipeForm = this.fb.group({
      'recipe-status': new FormControl(recipe?.relationships?.[`recipe-status`]?.data, Validators.required),
      'recipe-provider': new FormControl(recipe?.relationships?.[`recipe-provider`]?.data?.id ?
        recipe?.relationships?.[`recipe-provider`]?.data : null, Validators.required),
      attributes: this.fb.group({
        title: new FormControl(recipe.attributes[`title`], Validators.required),
        'media-url': new FormControl(recipe.attributes[`media-url`], Validators.required),
        ['number-of-guests']: new FormControl(recipe.attributes[`number-of-guests`] ?
          recipe.attributes[`number-of-guests`] : 4, [Validators.min(0), Validators.max(999)]),
        'description': new FormControl(recipe.attributes[`description`]),
        'preparation-time': new FormControl(recipe.attributes[`preparation-time`] ? recipe.attributes[`preparation-time`] : ''),
        'preheating-time': new FormControl(recipe.attributes[`preheating-time`]),
        'cooking-time': new FormControl(recipe.attributes[`cooking-time`]),
        'resting-time': new FormControl(recipe.attributes[`resting-time`]),
        difficulty: new FormControl(recipe.attributes[`difficulty`] ? recipe.attributes[`difficulty`] : 1, Validators.required),
        cost: new FormControl(recipe.attributes[`cost`] ? recipe.attributes[`cost`] : 1, Validators.required),
      }),
      'recipe-type': new FormControl(recipe.relationships[`recipe-type`].data.id ? recipe.relationships[`recipe-type`].data : null,
        Validators.required),
      ingredients: this.initIngredientFormArray(recipe),
      steps: this.initStepFormArray(recipe)
    });
    this.formSubscription = this.recipeForm.valueChanges.subscribe(() => {
      this.cdr.detectChanges();
    });
  }

  /**
   * For each Recipe Ingredient will create a form
   * that will be group in an form Array
   * @param recipe current recipe that 'll fill form
   */
  private initIngredientFormArray(recipe: Recipe): FormArray {
    const ingredients = this.fb.array([]);
    const recipeIngredients = recipe.relationships[`ingredients`].data;
    recipeIngredients.forEach((ingredient: Ingredient) => {
      ingredients.push(this.fb.group({
        id: new FormControl(ingredient.id),
        attributes: this.fb.group({
          quantity: new FormControl(ingredient.attributes.quantity, Validators.required),
          unit: new FormControl(ingredient.attributes.unit, Validators.required),
          name: new FormControl(ingredient.attributes.name, Validators.required)
        })
      }));
    });
    return ingredients;
  }

  /**
   * For each Recipe steps will create a form
   * that will be group in an form Array
   * @param recipe current recipe that 'll fill form
   */
  private initStepFormArray(recipe: Recipe): FormArray {
    const steps = this.fb.array([]);
    const recipeSteps = recipe.relationships[`recipe-steps`].data;
    recipeSteps.forEach((step: RecipeStep, index: number) => steps.push(this.fb.group({
      id: new FormControl(step.id),
      attributes: this.fb.group({
        'step-number': new FormControl(index),
        description: new FormControl(step.attributes.description)
      })
    })));
    return steps;
  }

  /**
   * Merge current recipe with form result
   * return an obsevable that'll update or create recipe
   */
  private updateRecipeAttributes(): Observable<Recipe> {
    const param = {
      include: ['ingredients', 'recipe-steps', 'recipe-status', 'recipe-provider', 'recipe-type']
    };
    this.recipe.attributes = { ... this.recipe.attributes, ...this.recipeForm.getRawValue().attributes };
    return this.recipe.save(param).pipe(map((res: DocumentResource<Recipe>) => {
      const recipe = this.recipeService.new();
      recipe.fill(res);
      return recipe[`data`] as Recipe;
    }));
  }

  /**
   * Update recipe's relationships Ingredient and  Steps
   * if at least one steps has changed will update the full list of step,
   * if at least one ingredient changed it 'll update the full list of ingredient,
   * it can update ingrdient list without updating steps list and vice versa.
   * @param currentRecipe to update this recipe already exist in backend it has to had an id
   */
  private updateRecipeRelationShips(currentRecipe: Recipe) {
    const formValue = this.recipeForm.getRawValue();
    return forkJoin([
      this.ingredientService.upsertIngredientList(this.recipeForm.get('ingredients').dirty ?
        this.ingredientService.formToIngredientMapper(currentRecipe, formValue.ingredients) : []),
      this.ingredientService.deleteIngredientList(this.deleteIngredientsList),
      this.recipeStepsService.createOrUpdateRelatedSteps(this.recipeForm.get('steps').dirty ?
        this.recipeStepsService.formToStepsMapper(currentRecipe, formValue.steps) : []),
      this.recipeStepsService.deleteStepList(this.deleteStepsList)
    ]);
  }

  /**
   * Update or create Recipe
   * it can only update changed part of a recipe
   */
  private upsertRecipe(): Observable<any> {
    // break upsert : we have to choose beetwen relationships and this field
    delete this.recipe.attributes[`ingredients-str`];
    this.handleRecipeStatusWorkflow();
    this.setRelationShipIfFormPartIsDirty('recipe-type');
    this.setRelationShipIfFormPartIsDirty('recipe-status');
    this.setRelationShipIfFormPartIsDirty('recipe-provider');
    /** update or create full recipe */
    if (this.recipeForm.get('attributes')?.dirty) {
      return this.updateRecipeAttributes().pipe(switchMap(recipe => {
        if (this.recipeForm.get('ingredients')?.dirty || this.recipeForm.get('steps')?.dirty) {
          // return a recipe only if it' a create if it's an update return void
          this.recipe = recipe ? recipe : this.recipe;
          return this.updateRecipeRelationShips(this.recipe).pipe(finalize(() => this.handleEmition(recipe)));
        }
        return of(recipe).pipe(finalize(() => this.handleEmition(recipe)));
      }));
    }
    /** update ingredient and steps relationships  */
    return this.updateRecipeRelationShips(this.recipe).pipe(finalize(() => this.handleEmition(this.recipe)));
  }

  private setRelationShipIfFormPartIsDirty(formPart: string): void {
    if (this.recipeForm.get(`${formPart}`)?.dirty) {
      // force full recipe update
      this.recipeForm.get('attributes').markAsDirty();
      this.recipe.addRelationship(this.recipeForm.get(`${formPart}`).value, `${formPart}`);
    }
  }

  /**
   * Emit when change has been done back
   * reset form's states
   **/
  private handleEmition(recipe: Recipe): void {
    this.recipeChange.emit(recipe ? recipe : this.recipe);
    this.recipeForm.markAsPristine();
  }

  private handleRecipeStatusWorkflow() {
    // use if we are patching recipe as an non Admin user
    if (!this.isAdmin) {
      this.recipeForm.get('recipe-status').patchValue(this.statusService.submittedValue);
      this.recipeForm.get('recipe-status').markAsDirty();
    }
  }
}



import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy, HostListener, OnDestroy, ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs';
import { BasketsService, GroceriesListsService, BasketStats } from '../../_services';
import { tap, skipWhile } from 'rxjs/operators';
import { PointOfSalesService } from '../../_services/point-of-sales.service';
import { PointOfSale } from '../../_models/point-of-sale';
import { AnalyticsService } from '../../_services/analytics.service';

@Component({
  selector: 'ng-miam-drawer',
  templateUrl: './drawer.component.html',
  styleUrls: ['./drawer.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DrawerComponent implements OnInit, OnDestroy {
  expand = false;
  recipesCount: number;
  jiggle = false;
  valid = false;
  loading = false;
  pos: PointOfSale;
  basketStats: BasketStats;

  private subscriptions = [];

  constructor(
    private cdr: ChangeDetectorRef,
    private listsService: GroceriesListsService,
    private posService: PointOfSalesService,
    private basketsService: BasketsService,
    private analyticsService: AnalyticsService
  ) { }

  ngOnInit() {
    this.subscriptions.push(
      this.listsService.list$.pipe(
        skipWhile(l => !l),
        tap(list => {
          if (list.attributes['recipes-infos'] && list.attributes['recipes-infos'].length > 0) {
            // Jiggle when recipes count changes
            if (this.recipesCount !== list.attributes['recipes-infos'].length) {
              this.doJiggle();
              // Open drawer when first recipe added
              if (this.recipesCount === 0 && list.attributes['recipes-infos'].length === 1) {
                this.show();
              }
            }
            this.recipesCount = list.attributes['recipes-infos'].length;
          } else {
            this.recipesCount = 0;
          }
          this.assessValid();
        })
      ).subscribe()
    );
    this.subscriptions.push(
      this.posService.pos$.pipe(
        skipWhile(pos => !pos),
        tap(pos => {
          this.pos = pos;
          this.assessValid();
        })
      ).subscribe()
    );
    this.subscriptions.push(
      this.basketsService.basketStats$.pipe(
        tap(stats => {
          this.basketStats = stats;
          this.cdr.detectChanges();
        })
      ).subscribe()
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub: Subscription) => sub.unsubscribe());
  }

  @HostListener('document:keydown.escape', ['$event'])
  onKeydownHandler(event: KeyboardEvent) {
    this.hide();
  }

  @HostListener('document:click', ['$event'])
  clickout(event) {
    const overlay = document.getElementById('miam-drawer-overlay');
    if (event.target === overlay) {
      this.hide();
    }
  }

  toggleExpand() {
    this.expand = !this.expand;
    this.expand ? this.show() : this.hide();
  }

  order() {
    if (this.loading) {
      return;
    }

    this.loading = true;
    this.cdr.detectChanges();
    this.subscriptions.push(this.basketsService.confirmBasket().subscribe(basket => {
      if (!basket.redirectUrl) {
        console.error('Basket not available on supplier Website');
      } else {
        window.location.assign(basket.redirectUrl);
      }
      this.loading = false;
      this.cdr.detectChanges();
    }));
  }

  private show() {
    this.expand = true;
    this.analyticsService.sendEvent('open-drawer');
    document.body.style.overflow = 'hidden';
    this.cdr.detectChanges();
  }

  private hide() {
    this.expand = false;
    this.analyticsService.sendEvent('close-drawer');
    document.body.style.overflow = 'auto';
    this.cdr.detectChanges();
  }

  private doJiggle() {
    this.jiggle = true;
    this.cdr.detectChanges();
    setTimeout(() => {
      this.jiggle = false;
      this.cdr.detectChanges();
    }, 500);
  }

  private assessValid() {
    this.valid = this.pos && this.recipesCount && this.recipesCount > 0;
    this.cdr.detectChanges();
  }
}

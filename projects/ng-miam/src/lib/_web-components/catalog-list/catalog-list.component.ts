import { Component, ViewEncapsulation, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy, OnInit } from '@angular/core';
import { DocumentCollection } from 'ngx-jsonapi';
import { BehaviorSubject, Subscription } from 'rxjs';
import { skipWhile} from 'rxjs/operators';
import { Recipe } from '../../_models/recipe';
import { RecipesService } from '../../_services';

@Component({
  selector: 'ng-miam-catalog-list',
  templateUrl: './catalog-list.component.html',
  styleUrls: ['./catalog-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CatalogListComponent implements OnDestroy, OnInit {
  currentRecipes$: BehaviorSubject<Recipe[]>;
  lastPage = 0;
  subscriptions: Subscription[];
  loading = true;
  filters = {};
  title = ''; // TODO: temporary, move to catalog header

  private observer: IntersectionObserver;

  constructor(
    private recipesService: RecipesService,
    private cdr: ChangeDetectorRef
  ) {
    this.subscriptions = [];
    this.currentRecipes$ = new BehaviorSubject<Array<Recipe>>([]);
  }

  ngOnInit() {
    this.initLoadOnScroll();
  }

  // If reused elesewhere, consider to make it a directive
  initLoadOnScroll() {
    this.observer = new IntersectionObserver(([entry]) => {
      if (entry.isIntersecting && this.currentRecipes$.getValue().length > 0) { this.loadMore(); }
    }, {});

    this.observer.observe(document.getElementById('miam-catalog-list__anchor'));
  }

  loadMore() {
    this.loading = true;
    this.lastPage++;
    this.loadRecipesPage(this.lastPage);
  }

  loadRecipesPage(page: number) {
    this.subscriptions.push(this.recipesService.all({
      remotefilter: this.filters,
      page: { size: 20, number: page },
      include: ['ingredients', 'recipe-steps', 'recipe-status', 'sponsors']
    }).pipe(
      skipWhile(resp => resp.is_loading)
    )
    .subscribe((result: DocumentCollection<Recipe>) => {
      this.currentRecipes$.next([...this.currentRecipes$.getValue(), ...result.data]);
      this.loading = false;
      this.cdr.detectChanges();
    }));
  }

  reloadPage() {
    this.lastPage = 0;
    this.currentRecipes$.next([]);
    this.loadMore();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
    this.observer.disconnect();
  }
}

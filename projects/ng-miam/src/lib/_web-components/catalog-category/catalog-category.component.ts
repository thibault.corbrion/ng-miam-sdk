import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnChanges, OnDestroy, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { DocumentCollection } from 'ngx-jsonapi';
import { Subscription } from 'rxjs';
import { skipWhile } from 'rxjs/operators';
import { Recipe } from '../../_models';
import { RecipesService } from '../../_services';
import { Icon } from '../../_types/icon.enum';
import * as _ from 'lodash';

const CARD_WIDTH = 346;
const SPACE_BETWEEN_CARD = 16;

@Component({
  selector: 'ng-miam-catalog-category',
  templateUrl: './catalog-category.component.html',
  styleUrls: ['./catalog-category.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CatalogCategoryComponent implements OnDestroy, OnChanges {
  @Input() filters: any; // Remotefilters for the request (ex: { cost: "3,eq", search:"pommes de terre"})
  @Input() title = '';
  @Output() displayList: EventEmitter<any>;

  @ViewChild('categoryContent') categoryContent: ElementRef;
  @ViewChild('categoryCards') categoryCards: ElementRef;

  subscriptions: Subscription[];
  recipes: Recipe[];
  isLoading: boolean;
  icon = Icon;
  displayableSize = 0;
  totalSize = 0;
  numberOfSections = 0;
  currentSection = 0;
  sectionSize = 0;

  constructor(
    private recipesService: RecipesService,
    private cdr: ChangeDetectorRef) {
    this.displayList = new EventEmitter();
    this.subscriptions = [];
    this.recipes = [];
    this.isLoading = true;
  }

  ngOnChanges(): void {
    if (this.filters) {
      this.loadRecipes();
    }
  }

  loadRecipes() {
    this.subscriptions.push(this.recipesService.all({
      page: { size: 20, number: 1 },
      remotefilter : this.filters,
      include: ['ingredients', 'recipe-steps', 'recipe-status', 'sponsors']
    }).pipe(
      skipWhile(resp => resp.is_loading)
    )
    .subscribe((result: DocumentCollection<Recipe>) => {
      // Shuffle the recipes so we don't have the same order everytime this category is shown
      this.recipes = _.shuffle(result.data);
      this.isLoading = false;
      this.cdr.detectChanges();
      this.createSections();
    }));
  }

  createSections() {
    this.displayableSize = this.categoryContent.nativeElement.clientWidth;
    this.totalSize = this.categoryCards.nativeElement.scrollWidth;
    const cardsBySection = Math.floor(this.displayableSize / CARD_WIDTH);
    this.sectionSize = cardsBySection * CARD_WIDTH;
    const sections = this.totalSize / this.sectionSize;
    this.numberOfSections = sections > Math.floor(sections) ? Math.floor(sections) : Math.floor(sections) - 1;
    this.cdr.detectChanges();
  }

  xTranslation() {
    if (this.currentSection !== this.numberOfSections) { return this.currentSection * this.sectionSize; }
    return this.totalSize - this.displayableSize + SPACE_BETWEEN_CARD;
  }

  slideLeft() {
    this.currentSection -= 1;
    this.cdr.detectChanges();
  }

  slideRight() {
    this.currentSection += 1;
    this.cdr.detectChanges();
  }

  displayListMode() {
    this.displayList.emit({title: this.title, filters: this.filters});
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}

import { Component, Output, EventEmitter, ChangeDetectionStrategy, ViewEncapsulation, Input } from '@angular/core';
import { Icon } from '../../_types/icon.enum';
import { Recipe } from '../../_models';


@Component({
  selector: 'ng-miam-recipe-addon',
  templateUrl: './recipe-addon.component.html',
  styleUrls: ['./recipe-addon.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RecipeAddonComponent {

  @Input() recipe: Recipe;
  @Output() hideAddon: EventEmitter<void>;

  icon = Icon;

  constructor() {
    this.hideAddon = new EventEmitter();
   }
}

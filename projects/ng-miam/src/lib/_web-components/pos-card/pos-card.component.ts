import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  OnDestroy,
  Output,
  EventEmitter,
  ViewEncapsulation
} from '@angular/core';
import { PointOfSale } from '../../_models/point-of-sale';
import { Basket, BasketCompletion } from '../../_models/basket';
import { BasketsService, GroceriesListsService } from '../../_services';
import { switchMap, skipWhile } from 'rxjs/operators';
import { Subscription, Observable } from 'rxjs';

@Component({
  selector: 'ng-miam-pos-card',
  templateUrl: './pos-card.component.html',
  styleUrls: ['./pos-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class PosCardComponent implements OnInit, OnDestroy {
  @Input() pos: PointOfSale;
  @Input() selected = false;
  @Input() hoverText: string;
  @Output() select = new EventEmitter<PointOfSale>();

  completion: BasketCompletion;
  gaugeColor: string;
  gaugeSize: string;

  private subscriptions: Subscription[] = [];

  constructor(
    public cdr: ChangeDetectorRef,
    private listsService: GroceriesListsService,
    private basketsService: BasketsService
  ) { }

  ngOnInit() {
    let basketObs: Observable<Basket>;
    if (this.selected) {
      basketObs = this.basketsService.basket$.pipe(
        skipWhile(b => !b)
      );
    } else {
      basketObs = this.listsService.list$.pipe(
        skipWhile(l => !l),
        switchMap(list => this.basketsService.fetchForListAndPos(list.id, this.pos.id))
      );
    }
    this.subscriptions.push(basketObs.subscribe(basket => {
      this.calculateGauge(basket);
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub: Subscription) => sub.unsubscribe());
  }

  private calculateGauge(basket: Basket) {
    if (!basket.completion) {
      return null;
    }

    this.completion = basket.completion;
    const ratio = this.completion.found / (this.completion.total || 1);
    // const baseSize = this.mobile ? 210 : 125;
    const baseSize = 125;
    this.gaugeSize = `${baseSize * ratio}px`;
    if (ratio >= 0.85) {
      this.gaugeColor = '#40b6aa';
    } else if (ratio >= 0.6) {
      this.gaugeColor = '#f6a825';
    } else {
      this.gaugeColor = '#ef7612';
    }
    this.cdr.detectChanges();
  }
}

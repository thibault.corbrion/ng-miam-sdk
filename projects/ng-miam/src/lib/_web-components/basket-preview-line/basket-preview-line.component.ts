import {
  Component,
  Input,
  Output,
  EventEmitter,
  ViewEncapsulation,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  OnInit
} from '@angular/core';
import { BasketPreviewLine } from '../../_models/basket-preview-line';
import { BasketEntriesService } from '../../_services/basket-entries.service';
import { forkJoin, BehaviorSubject, Subscription } from 'rxjs';
import { BasketEntry } from '../../_models/basket-entry';
import { GroceriesListsService } from '../../_services/groceries-lists.service';
import * as _ from 'lodash';
import { BasketsService, ContextService, PointOfSalesService, RecipesService } from '../../_services';
import { switchMap, take, tap } from 'rxjs/operators';
import { AnalyticsService } from '../../_services/analytics.service';
import { Icon } from '../../_types/icon.enum';
import { stopEventPropagation } from '../../_utils/event.utils';

@Component({
  selector: 'ng-miam-basket-preview-line',
  templateUrl: './basket-preview-line.component.html',
  styleUrls: ['./basket-preview-line.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BasketPreviewLineComponent implements OnInit, OnDestroy {
  @Input() line: BasketPreviewLine;
  @Input() uniqueLine: boolean;
  @Input() blockStates: BehaviorSubject<{ currentOpenLine: string, overlayOpen: boolean, lineComponent: BasketPreviewLineComponent }>;
  @Output() removed = new EventEmitter<BasketPreviewLine>();
  @Output() countChanged = new EventEmitter<BasketPreviewLine>();
  @Output() replaceItem = new EventEmitter<BasketPreviewLine>();
  @Output() showRecipe = new EventEmitter<{ id: string, guests: number }>();

  entriesLines$ = new BehaviorSubject<BasketPreviewLine[]>(null);
  expanded = false;
  loading = true;
  disableItemSelector = false;
  selectItem = false;
  icon = Icon;

  private subscriptions: Subscription[] = [];

  constructor(
    public cdr: ChangeDetectorRef,
    private recipesService: RecipesService,
    private listsService: GroceriesListsService,
    private basketsService: BasketsService,
    private basketEntriesService: BasketEntriesService,
    private analyticsService: AnalyticsService,
    protected posService: PointOfSalesService,
    private contextService: ContextService
  ) { }

  // TODO: can we remove the condition on uniqueLine?
  ngOnInit() {
    if (this.uniqueLine) {
      this.expanded = true;
      this.buildEntriesLines();
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub: Subscription) => sub.unsubscribe());
  }

  changeCount(count: number) {
    if (this.line.count === count) {
      return;
    }

    this.line.count = count;
    if (this.line.hasEntries() || this.line.displayMode) {
      this.countChanged.emit(this.line);
    } else {
      this.handleSublineCountChange(count);
    }
  }

  handleSublineCountChange(count: number) {
    if (count === 0) {
      this.removed.emit(this.line);
      return;
    }
    this.line.record.attributes.quantity = count;
    this.line.record.updateTotalPrice();
    this.analyticsService.sendEvent('change-item-quantity');
    this.subscriptions.push(this.line.record.save().pipe(
      switchMap(() => this.basketsService.loadBasket()),
      take(1)
    ).subscribe());
  }

  toggleExpanded(event) {
    if (
      !this.line.hasEntries() ||
      this.uniqueLine ||
      event.target.parentElement.className === 'input-container' ||
      event.target.parentElement.className === 'remove' ||
      event.target.parentElement.nodeName === 'svg'
    ) {
      return;
    }

    this.expanded = !this.expanded;
    this.cdr.detectChanges();
    if (!this.entriesLines$.value) {
      this.buildEntriesLines();
    }
  }

  deleteEntry(entry: BasketEntry) {
    // Remove entry from line entries array
    // TODO: handle other than found
    _.remove(this.line.entries.found, e => e.id === entry.id);
    this.buildEntriesLines();

    // Remove related groceries entry (update status to "deleted") and reload list
    const gEntry = entry.relationships['groceries-entry'].data;
    gEntry.attributes.status = 'deleted';
    this.subscriptions.push(
      gEntry.save().pipe(
        switchMap(() => this.basketsService.loadBasket()),
        take(1)
      ).subscribe()
    );
  }

  changeProduct(line: BasketPreviewLine) {
    this.replaceItem.emit(line);
    this.blockStates.next({ currentOpenLine: line.id, overlayOpen: true, lineComponent: this });
    this.selectItem = true;
    this.cdr.detectChanges();
  }

  productChoosen(line?: BasketPreviewLine) {
    this.replaceItem.emit(line);
    this.selectItem = false;
    if (line) { this.disableItemSelector = true; }
    this.blockStates.next({ currentOpenLine: null, overlayOpen: false, lineComponent: null });
    this.cdr.detectChanges();
  }

  buildEntriesLines() {
    const entries = _.concat(
      this.line.entries.found,
      this.line.entries.notFound,
      this.line.entries.oftenDeleted,
      this.line.entries.removed
    );
    entries.map((entry: BasketEntry) => {
      entry.setItemsSelection();
      entry.updateTotalPrice();
    });
    this.entriesLines$.next(
      entries
        .filter(entry => this.line.entries.found.map(e => e.id).includes(entry.id))
        .map(entry => BasketPreviewLine.fromBasketEntry(entry))
    );
    this.loading = false;
    this.cdr.detectChanges();
  }

  addEntry(entry: BasketEntry) {
    const gEntry = entry.relationships['groceries-entry'].data;
    gEntry.attributes.status = 'active';
    this.subscriptions.push(
      gEntry.save().pipe(
        switchMap(() => this.basketsService.loadBasket()),
        take(1)
      ).subscribe()
    );
  }

  trackByItemId(index: number, line: BasketPreviewLine) {
    return line ? line.record['selected-item-id'] + '-' + line.record.attributes.quantity : null;
  }

  toggleRecipeDisplay(event, addRecipe = false) {
    if (!this.line.isRecipe) {
      return;
    }
    if (addRecipe) {
      this.posService.isPosValid().subscribe(isV => {
        if (isV) {
          this.listsService.appendRecipeToList(this.line.id, this.line.count);
          this.recipesService.display(this.line.id, this.line.count, addRecipe);
        } else if (this.contextService.invalidPosCallback) {
          this.contextService.invalidPosCallback();
        }
      });
    } else {
      this.recipesService.display(this.line.id, this.line.count);
    }
    event.stopPropagation();
  }

  removeLine(event){
    stopEventPropagation(event);
    this.removed.emit(this.line);
  }
}

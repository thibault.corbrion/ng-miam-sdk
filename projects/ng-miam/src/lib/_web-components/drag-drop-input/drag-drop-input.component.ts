import { ElementRef, OnChanges } from '@angular/core';
import { Component, Output, EventEmitter, ChangeDetectorRef, ViewEncapsulation, ChangeDetectionStrategy, Input, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Icon } from '../../_types/icon.enum';

@Component({
  selector: 'ng-miam-drag-drop-input',
  templateUrl: './drag-drop-input.component.html',
  styleUrls: ['./drag-drop-input.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DragDropInputComponent implements OnChanges {

  @Input() photoMode: boolean;
  @Input() formControl: FormControl;
  @Input() imageUrl: string | ArrayBuffer;

  @Output() onFileDropped = new EventEmitter<any>();

  @ViewChild('fileInput') fileInput: ElementRef;

  public icon = Icon;

  public isDragover: boolean;

  constructor(private cdr: ChangeDetectorRef) {

  }

  ngOnChanges() {
    this.cdr.detectChanges();
  }

  onDragOver(event): void {
    event.preventDefault();
    event.stopPropagation();
    this.isDragover = true;
    this.cdr.detectChanges();
  }

  onDragLeave(event): void {
    event.preventDefault();
    event.stopPropagation();
    this.isDragover = false;
    this.cdr.detectChanges();
  }

  onDrop(event): void {
    event.preventDefault();
    event.stopPropagation();
    this.isDragover = false;
    this.uploadFile(event.dataTransfer.files);
  }

  fileSelected(event){
    event.preventDefault();
    event.stopPropagation();

    this.uploadFile(event.target.files)
  }

  uploadFile(files: FileList) {
    this.onFileDropped.emit(files);
    if (this.photoMode && files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(files[0]);
      reader.onload = event => {
        this.imageUrl = reader.result;
        this.formControl.patchValue(this.imageUrl);
        this.cdr.detectChanges();
      };
    }
  }

  openExplorer() {
    this.fileInput.nativeElement.click();
  }

}
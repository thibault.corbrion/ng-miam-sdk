import {Component, EventEmitter, Input, Output, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import {RecipesService} from '../../_services/recipes.service';
import {MiamRecipe} from '../../_types/recipe';
import { catchError, tap, switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { GroceriesList } from '../../_models/groceries-list';
import { GroceriesListsService } from '../../_services';

@Component({
  selector: 'ng-miam-order-button',
  templateUrl: './order-button.component.html',
  styleUrls: ['./order-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OrderButtonComponent {
  @Input() recipe: MiamRecipe;
  @Input() source: string;
  @Input() text = 'Order with Miam';
  @Input() openOnSave = false;
  @Output() saved: EventEmitter<any> = new EventEmitter<any>();
  @Output() error: EventEmitter<any> = new EventEmitter<any>();

  loadingT;
  loading = false;
  private readonly loadingAnimT = 550;

  constructor(
    private cdr: ChangeDetectorRef,
    private recipesService: RecipesService,
    private listsService: GroceriesListsService
  ) { }

  createRecipe() {
    this.setLoading(true);
    let obs: Observable<GroceriesList>;
    if (this.recipe) {
      obs = this.recipesService.getOrCreate(this.recipe, this.openOnSave);
    } else {
      obs = this.recipesService.createFromUrl(this.source, document.location.href).pipe(
        switchMap(r => this.listsService.appendRecipeToList(r.id, r.modifiedGuests))
      );
    }
    obs.pipe(
      catchError(e => {
        this.error.emit(e);
        return e;
      }),
      tap(list => {
        this.saved.emit(list);
        this.setLoading(false);
      })
    ).subscribe();
  }

  private setLoading(state: boolean) {
    clearTimeout(this.loadingT);
    if (!state) {
      this.loadingT = setTimeout(
        () => {
          this.loading = state;
          this.cdr.detectChanges();
        },
        this.loadingAnimT
      );
    } else {
      this.loading = true;
      this.cdr.detectChanges();
    }
  }
}

import { Component, ViewEncapsulation, ChangeDetectionStrategy, ViewChild, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { Icon } from '../../_types/icon.enum';
import { CatalogListComponent } from '../catalog-list/catalog-list.component';

interface CatalogFilter {
  name: string;
  value: boolean;
  text: string;
}

@Component({
  selector: 'ng-miam-recipe-catalog',
  templateUrl: './recipe-catalog.component.html',
  styleUrls: ['./recipe-catalog.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RecipeCatalogComponent {
  @ViewChild(CatalogListComponent) private catalogList: CatalogListComponent;

  filters: {
    // type: CatalogFilter[],
    difficulty: CatalogFilter[],
    cost: CatalogFilter[],
    time: CatalogFilter[],
  };
  searchString = '';
  isListMode = false;
  icon = Icon;
  currentCategoryFilter = {};
  currentCategoryTitle = '';

  constructor( private cdr: ChangeDetectorRef ) {
    this.resetFilters();
  }

  // Used on load to build the filters attribute, and on click on the link to reset the filters
  resetFilters() {
    const shouldReload = this.filters !== undefined;
    this.filters = {
      // type: [
      //   {name: recipeTypesService.getType(1).id, value: false, text: 'Entrées'},
      //   {name: recipeTypesService.getType(2).id, value: false, text: 'Plats'},
      //   {name: recipeTypesService.getType(3).id, value: false, text: 'Desserts'}
      // ],
      difficulty: [
        {name: '1', value: false, text: 'Facile'},
        {name: '2', value: false, text: 'Moyen'},
        {name: '3', value: false, text: 'Difficile'}
      ],
      cost: [
        {name: '1', value: false, text: 'Bas'},
        {name: '2', value: false, text: 'Moyen'},
        {name: '3', value: false, text: 'Élevé'}
      ],
      time: [
        {name: '15', value: false, text: '15m'},
        {name: '30', value: false, text: '30m'},
        {name: '60', value: false, text: '1h'},
        {name: '120', value: false, text: '2h'}
      ],
    };
    // Reload the catalogList when the function was called to reset the filters
    if (shouldReload) { this.reloadList(); }
  }

  // There is no reason for having multiple time chackboxes checked at the same time so on click we uncheck all before model change
  uncheckTimesOnChange(event: any) {
    this.filters.time.forEach(time =>  time.value = false);
  }

  reloadList() {
    this.isListMode = true;
    this.cdr.detectChanges();
    if (this.catalogList) {
      this.catalogList.title = this.currentCategoryTitle === '' ? 'Ma recherche' : this.currentCategoryTitle;
      this.catalogList.filters = this.buildRemoteFilters();
      this.catalogList.reloadPage();
    }
  }

  switchToListMode(event: {title: string, filters: any}) {
    this.currentCategoryFilter = event.filters;
    this.currentCategoryTitle = event.title;
    this.reloadList();
  }

  switchToCategoriesMode() {
    this.isListMode = false;
    this.filters = undefined;
    this.currentCategoryTitle = '';
    this.currentCategoryFilter = {};
    this.resetFilters();
    this.cdr.detectChanges();
  }

  buildRemoteFilters() {
    const difficulty = this.buildRemoteFilter(this.filters.difficulty, '-');
    const cost = this.buildRemoteFilter(this.filters.cost, '-');
    const totalTime = this.buildRemoteFilter(this.filters.time, ',');

    const filters = {
      ...(difficulty.length > 0) ? { difficulty: difficulty + ',eq' } : {},
      ...(cost.length > 0) ? { cost: cost + ',eq' } : {},
      ...(totalTime.length > 0) ? { 'total-time': totalTime } : {},
      ...this.currentCategoryFilter, // Double reason to not have a filter which is the same as a category -> this part won't like it
      search: this.searchString
    };
    return filters;
  }

  buildRemoteFilter(filter: CatalogFilter[], separator: string) {
    let result = '';
    for (const f of filter) {
      if (f.value) {
        if (result.length > 0) { result += separator; }
        result += f.name;
      }
    }
    return result;
  }
}

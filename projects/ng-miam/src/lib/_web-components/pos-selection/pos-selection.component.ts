import {
  Component,
  OnInit,
  Input,
  ChangeDetectorRef,
  OnDestroy,
  HostListener,
  ChangeDetectionStrategy,
  ViewEncapsulation,
  ViewChild
} from '@angular/core';
import { PointOfSalesService } from '../../_services/point-of-sales.service';
import { tap, skipWhile } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { PointOfSale } from '../../_models/point-of-sale';
import { PosCardComponent } from '../pos-card/pos-card.component';
import { ContextService } from '../../_services/context.service';
import { AnalyticsService } from '../../_services/analytics.service';
import { Icon } from '../../_types/icon.enum';

@Component({
  selector: 'ng-miam-pos-selection',
  templateUrl: './pos-selection.component.html',
  styleUrls: ['./pos-selection.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class PosSelectionComponent implements OnInit, OnDestroy {
  @Input() blockTitle = 'Mon magasin';
  @ViewChild('selectedPosCard') selectedPosCard: PosCardComponent;
  expand = true;
  loading = false;
  replacing = true;
  address: string;
  sorryCaption: string;
  selectedPos: PointOfSale;
  candidatePos: PointOfSale[];
  corsIssues = false;
  icon = Icon;

  private subscriptions: Subscription[];

  constructor(
    private cdr: ChangeDetectorRef,
    private contextService: ContextService,
    public posService: PointOfSalesService,
    private analyticsService: AnalyticsService
  ) { }

  ngOnInit() {
    this.subscriptions = [
      this.contextService.getCORSIssuesState().subscribe(res => {
        this.corsIssues = res;
        this.cdr.detectChanges();
      }),
      this.posService.pos$.pipe(
        skipWhile(pos => !pos),
        tap(pos => {
          this.selectedPos = pos;
          this.endReplace();
        })
      ).subscribe()
    ];
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub: Subscription) => sub.unsubscribe());
  }

  toggle() {
    this.expand = !this.expand;
    this.expand ? this.show() : this.hide();
  }

  show() {
    this.expand = true;
    this.cdr.detectChanges();
  }

  hide() {
    this.expand = false;
    this.cdr.detectChanges();
  }

  startReplace() {
    this.replacing = true;
    this.cdr.detectChanges();
    const input = document.getElementById('address-input') as HTMLInputElement;
    input.focus();
    if (this.address) {
      input.setSelectionRange(0, this.address.length);
    }
  }

  replace(pos: PointOfSale) {
    this.analyticsService.sendEvent('select-pos');
    this.subscriptions.push(
      this.posService.loadPos(pos.id).subscribe(() => this.cdr.detectChanges())
    );
    this.endReplace();
  }

  endReplace() {
    this.replacing = false;
    this.cdr.detectChanges();
  }

  @HostListener('document:keydown.enter')
  searchAddress() {
    if (!this.address) {
      return null;
    }

    this.candidatePos = null;
    this.loading = true;
    this.cdr.detectChanges();
    this.posService.all({ include: ['supplier'], remotefilter: { address: this.address } }).pipe(
      skipWhile(result => result.is_loading),
      tap(result => {
        this.candidatePos = result.data;
        this.sorryCaption = `Désolé, nous n'avons détecté aucun magasin à proximité de <i>${this.address}</i>.`;
        this.loading = false;
        this.cdr.detectChanges();
      })
    ).subscribe();
  }

}

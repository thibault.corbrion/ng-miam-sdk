import { Component, ViewEncapsulation, ChangeDetectorRef, ChangeDetectionStrategy, Input, OnChanges, Inject, Output, EventEmitter } from '@angular/core';
import { AbstractRecipeCardComponent } from '../../_components/abstracts/abstract-recipe-card.component';
import { RecipesService, GroceriesListsService, ContextService, RecipeEventsService } from '../../_services';
import { PointOfSalesService } from '../../_services/point-of-sales.service';
import { Recipe } from '../../_models/recipe';
import { AnalyticsService } from '../../_services/analytics.service';

/**
 * This is an angular component design to be a web component
 * that's why we use onPushStrategie with the changeDetetectorRef
 * to avoid common issue
 * guide -> https://netbasal.com/a-comprehensive-guide-to-angular-onpush-change-detection-strategy-5bac493074a4
 * and alsow we use ShadowDom to protect style of our component
 * and alsow style of the parent aplication that will use it
 * doc -> https://angular.io/api/core/ViewEncapsulation
 */
@Component({
  selector: 'ng-miam-display-recipe-card',
  templateUrl: './display-recipe-card.component.html',
  styleUrls: ['./display-recipe-card.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
//TODO rename display card to recipe card
export class DisplayRecipeCardComponent extends AbstractRecipeCardComponent implements OnChanges {
  @Input() recipe: Recipe;
  @Output() removedFromFavorite: EventEmitter<any> = new EventEmitter();
  @Output() displayed = new EventEmitter();
  @Output() guestsChanged = new EventEmitter<number>();

  constructor(
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
    @Inject(RecipesService) recipeService: RecipesService,
    @Inject(RecipeEventsService) recipeEventsService: RecipeEventsService,
    @Inject(GroceriesListsService) groceriesListsService: GroceriesListsService,
    @Inject(PointOfSalesService) pointOfSalesService: PointOfSalesService,
    @Inject(ContextService) contextService: ContextService,
    private analyticsService: AnalyticsService) {
    super(cdr, recipeService, recipeEventsService, groceriesListsService, pointOfSalesService, contextService);

    this.subscriptions.push(this.recipeService.displayedRecipeChanged.subscribe(() => this.cdr.detectChanges()));
  }

  ngOnChanges() {
    if (this.recipe != null) {
      this.initIngredientsString();
      this.recipe.modifiedGuests ||= +this.recipe.guests;
    }
  }

  removeFromFavorite() {
    this.removedFromFavorite.emit(this.recipe);
  }

  openRecipe() {
    this.recipeService.displayObject(this.recipe, this.recipe.modifiedGuests);
    this.displayed.emit();
  }

  updateGuests(count) {
    this.recipe.modifiedGuests = count;
    this.guestsChanged.emit(count);
    this.cdr.detectChanges();
  }
}

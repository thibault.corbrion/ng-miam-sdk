import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Observable, Subscription, of } from 'rxjs';
import { BasketPreviewLine } from '../../_models';
import { RecipesService, BasketsService } from '../../_services';
import { map, skipWhile, tap, switchMap, catchError } from 'rxjs/operators';
import { Icon } from '../../_types/icon.enum';
import { RecipeSelectionLogsService } from '../../_services/recipe-selection-logs.service';

const CALENDAR_FR = {
  sameDay: "[aujourd'hui]",
  lastDay: '[hier]',
  lastWeek: 'dddd [dernier]',
  sameElse: '[le] DD MMMM YYYY'
};

@Component({
  selector: 'ng-miam-recipes-history',
  templateUrl: './recipes-history.component.html',
  styleUrls: ['./recipes-history.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class RecipesHistoryComponent implements OnInit, OnDestroy {
  recipesGroups: { date: string, lines: Observable<BasketPreviewLine>[], show: boolean }[] = [];
  icon = Icon;
  pageNumber = 1;
  totalPages;
  private subscriptions: Subscription[] = [];

  constructor(
    private cdr: ChangeDetectorRef,
    private recipesService: RecipesService,
    private recipeSelectionLogsService: RecipeSelectionLogsService,
    private basketsService: BasketsService
  ) { }

  ngOnInit(): void {
    this.prepareGroups();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  toggleGroup(group) {
    group.show = !group.show;
    this.cdr.detectChanges();
  }

  page(increment) {
    const newPage = this.pageNumber + increment;
    if (newPage > 0 && newPage <= this.totalPages) {
      this.pageNumber = newPage;
      this.subscriptions.push(this.fetchLogs().subscribe());
    }
  }

  private prepareGroups() {
    this.subscriptions.push(
      this.basketsService.basket$.pipe(
        skipWhile(basket => !basket),
        switchMap(() => this.fetchLogs())
      ).subscribe()
    );
  }

  private fetchLogs(): Observable<any> {
    return this.recipeSelectionLogsService.all({ sort: ['-date'], page: { number: this.pageNumber } }).pipe(
      catchError(result => {
        if (!result.errors || result.errors.includes('User not authenticated')) {
          console.log('[Miam] hiding recipes selection history (no user logged in)');
          return of({ is_loading: false, data: [] });
        }
      }),
      skipWhile(result => result.is_loading),
      tap(result => {
        this.setPagination(result.meta);
        this.recipesGroups = result.data.map(log => {
          return {
            show: false,
            date: log.date.locale('fr').calendar(null, CALENDAR_FR),
            lines: log.recipesInfos.map(info => this.prepareLine(info))
          };
        });
        this.cdr.detectChanges();
      })
    );
  }

  private setPagination(page: { number, size, total_resources }) {
    if (page) {
      this.pageNumber = page.number;
      this.totalPages = page.total_resources / page.size;
    } else {
      this.pageNumber = 1;
      this.totalPages = null;
    }
  }

  private prepareLine(recipeInfo: { id: string, guests: string }): Observable<BasketPreviewLine> {
    return this.recipesService.get(recipeInfo.id, { include: ['ingredients', 'recipe-steps', 'recipe-status', 'recipe-type'] }).pipe(
      skipWhile(r => r.is_loading),
      map(recipe => {
        const line = BasketPreviewLine.fromRecipe(recipe, recipe.ingredients.length);
        line.displayMode = true;
        setTimeout(() => this.cdr.detectChanges()); // ensures the line will be reloaded after the observable resolves
        return line;
      })
    );
  }

}

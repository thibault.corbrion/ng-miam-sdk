
export const stopEventPropagation = (event: Event):void => {
  event.stopPropagation();
  event.preventDefault();
}

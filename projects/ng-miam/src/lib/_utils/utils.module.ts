import { NgModule } from '@angular/core';
import { ReadableFloatNumberPipe } from '.';
import { EllipsisPipe } from '.';
import { SafePipe } from './safePipe/safe.pipe';

@NgModule({
  imports: [
  ],
  declarations: [
    ReadableFloatNumberPipe,
    EllipsisPipe,
    SafePipe
  ],
  exports: [
    ReadableFloatNumberPipe,
    EllipsisPipe,
    SafePipe
  ]
})
export class UtilsModule {
  constructor() {}
}

import { Resource, DocumentResource } from 'ngx-jsonapi';
import { PointOfSale } from './point-of-sale';
import { Recipe } from './recipe';
import { RecipeProvider } from './recipe-provider';
import { Supplier } from './supplier';


export class RecipeShare extends Resource {

    public attributes = {
        "status": ''
    };

    public relationships = {
        shareable: new DocumentResource<Recipe | RecipeProvider>(),
        destination: new DocumentResource<PointOfSale | Supplier>()
    };

    public get isShared(): boolean {
        return this.attributes &&  this.attributes.status === 'shared';
      }
    }

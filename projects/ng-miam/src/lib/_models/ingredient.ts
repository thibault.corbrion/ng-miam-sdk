import { Resource, DocumentResource } from "ngx-jsonapi";
import { Recipe } from "./recipe";
import { QuantityUnit } from "./quantity-unit";

export class Ingredient extends Resource {
  public attributes = {
    'name': '',
    'quantity': '',
    'unit': '',
    'active': true
  };

  public relationships = {
    'recipe': new DocumentResource<Recipe>(),
    'quantity-unit': new DocumentResource<QuantityUnit>()
  }

  public get quantity(): string {
    if (this.attributes.quantity) {
      return parseFloat(this.attributes.quantity).toLocaleString();
    } else {
      return '1';
    }
  }
}

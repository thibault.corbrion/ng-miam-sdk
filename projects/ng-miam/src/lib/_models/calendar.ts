import { DocumentCollection, Resource } from 'ngx-jsonapi';
import { CalendarEvent } from './calendar-event';

export class Calendar extends Resource {
  attributes = {
    'user-id': '',
    'allow-modification': null
  };
  relationships = {
    'calendar-events': new DocumentCollection<CalendarEvent>()
  };
}

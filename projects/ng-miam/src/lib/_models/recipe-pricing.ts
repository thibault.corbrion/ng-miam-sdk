export class RecipePricing {
  price: number;
  serves: number;
  price_per_serve: number;

  constructor(attrs = {}) {
    Object.assign(this, attrs);
  }
}
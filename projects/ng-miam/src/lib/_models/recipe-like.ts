import { DocumentResource, Resource } from 'ngx-jsonapi';
import { Recipe } from './recipe';

export class RecipeLike extends Resource {
  public attributes = {
    'user-id': '',
    'is-past': false
  };

  public relationships = {
    recipe: new DocumentResource<Recipe>()
  };
}

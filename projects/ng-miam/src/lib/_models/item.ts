import { Resource } from "ngx-jsonapi";

export class Item extends Resource {
  public attributes = {
    'name': '',
    'status': '',
    'description': '',
    'brand': '',
    'product-page': '',
    'image': '',
    'unit-price': '',
    'currency': '',
    'capacity-unit': '',
    'capacity-volume': '',
    'capacity-factor': 1,
    'created-at': '',
    'updated-at': '',
    'promoted': false,
    'variable-capacity': false
  };
  private _inBasket: boolean;
  private _variableCapacity: string;

  public getPrice(): number {
    return (this.attributes['unit-price'] ? parseFloat(this.attributes['unit-price']) : 0.0);
  }

  public set inBasket(inBasket: boolean) {
    this._inBasket = inBasket;
  }

  public get inBasket(): boolean {
    return this._inBasket;
  }

  public get capacity(): string {
    if (this.hasVariableCapacity) {
      return this.variableCapacity;
    } else {
      return this.fixedCapacity;
    }
  }

  public get hasVariableCapacity(): boolean {
    return !!(this.attributes['variable-capacity'] && this.variableCapacity);
  }

  public get fixedCapacity(): string {
    let cap = this.capacityVolume;
    if (this.capacityFactor && this.capacityFactor > 1) {
      cap = `${this.capacityFactor} x ${cap}`;
    }
    if (this.capacityUnit?.length > 0) {
      cap = `${cap} ${this.attributes['capacity-unit']}`;
    }
    return cap;
  }

  public get variableCapacity(): string {
    return this._variableCapacity;
  }

  public set variableCapacity(cap: string) {
    if (cap && cap.length > 0) {
      this._variableCapacity = `${cap} ${this.capacityUnit}`;
    }
  }

  public get capacityVolume(): string {
    return this.attributes['capacity-volume'];
  }

  public get capacityFactor(): number {
    return this.attributes['capacity-factor'];
  }

  public get capacityUnit(): string {
    return this.attributes['capacity-unit'];
  }

  public get pftPlages(): number[] {
    return this.attributes['pft-plages'];
  }
}

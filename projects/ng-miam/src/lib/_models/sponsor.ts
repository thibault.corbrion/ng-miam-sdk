
import {  Resource } from 'ngx-jsonapi';

export class Sponsor extends Resource {

  public attributes = {
    name: '',
    'logo-url': ''
  };

}

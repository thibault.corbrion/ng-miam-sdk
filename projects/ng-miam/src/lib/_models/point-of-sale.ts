import { Resource, DocumentCollection, DocumentResource } from 'ngx-jsonapi';
import { Item } from './item';
import { Supplier } from './supplier';

export class PointOfSale extends Resource {
  public attributes = {
    name: '',
    settings: {
      drive: {
        enabled: true,
        logo: ''
      },
      delivery: {
        enabled: true,
        logo: ''
      },
      mobile_warning: false
    },
    'active-items-count': 0,
    distance: 0.0,
    address: '',
    latitude: '',
    longitude: ''
  };

  public relationships = {
    supplier: new DocumentResource<Supplier>(),
    items: new DocumentCollection<Item>()
  };

  get references(): string {
    return new Intl.NumberFormat('Fr-fr').format(this.attributes['active-items-count']);
  }

  get driveEnabled(): boolean {
    const settings = this.attributes.settings;
    return settings && settings.drive && settings.drive.enabled;
  }

  get deliveryEnabled(): boolean {
    const settings = this.attributes.settings;
    return settings && settings.delivery && settings.delivery.enabled;
  }
}

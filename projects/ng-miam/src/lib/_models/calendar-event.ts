import { CalendarEvent as AngularCalendarEvent } from 'angular-calendar';
import { DocumentResource, Resource } from 'ngx-jsonapi';
import { Calendar } from './calendar';
import { Recipe } from './recipe';

export class CalendarEvent extends Resource implements AngularCalendarEvent {
  attributes = {
    'event-date': ''
  };
  relationships = {
    calendar: new DocumentResource<Calendar>(),
    recipe: new DocumentResource<Recipe>()
  };

  // Attributes from the interface that should always be true
  allDay = true;
  draggable = true;

  get recipe(): Recipe {
    return this.relationships.recipe && this.relationships.recipe.data;
  }

  // Override the attribute start from interface
  get start(): Date {
    return new Date(this.attributes['event-date']);
  }

  set start(date: Date) {
    this.attributes['event-date'] = date.toString();
  }

  // Override the attribute title from interface
  get title(): string {
    return this.recipe.title;
  }
}

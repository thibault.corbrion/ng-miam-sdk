import { Resource, DocumentResource } from "ngx-jsonapi";

export class QuantityUnit extends Resource {
  attributes = {
    'name': '',
    'plural': ''
  };
  relationships = {
    'reference-quantity-unit': new DocumentResource<QuantityUnit>()
  };
}

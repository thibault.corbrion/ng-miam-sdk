import { Resource, DocumentCollection, DocumentResource } from 'ngx-jsonapi';
import { BasketEntry } from './basket-entry';
import { PointOfSale } from './point-of-sale';
import { environment } from '../environment';

export interface BasketCompletion {
  total: number;
  found: number;
}

export class Basket extends Resource {
  public attributes = {
    name: '',
    confirmed: false,
    completion: { total: 0, found: 0},
    token: null // token exposed only on basket confirmation (PATCH confirmed=true)
  }

  public relationships = {
    'basket-entries': new DocumentCollection<BasketEntry>(),
    'point-of-sale': new DocumentResource<PointOfSale>()
  };

  public get redirectUrl(): string {
    if (!this.id) {
      return null;
    }

    return `${environment.miamAPI}/api/v1/baskets/${this.id}/redirect`;
  }

  get completion(): BasketCompletion {
    return this.attributes.completion;
  }

  get entries(): BasketEntry[] {
    return this.relationships['basket-entries'] && this.relationships['basket-entries'].data;
  }
}

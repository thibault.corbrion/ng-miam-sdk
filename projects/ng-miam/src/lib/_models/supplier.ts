import { Resource, DocumentCollection } from 'ngx-jsonapi';
import { Item } from './item';
import { PointOfSale } from './point-of-sale';

export class Supplier extends Resource {
  public attributes = {
    name: '',
    description: '',
    logo: '',
    settings: {},
  };

  public relationships = {
    'point-of-sales': new DocumentCollection<PointOfSale>(),
    items: new DocumentCollection<Item>()
  };

  get logo(): string {
    let logo = this.attributes.logo;
    if (logo.match(/^assets\//)) {
      logo = logo.replace('assets', 'https://miam.tech/fr/assets');
    }
    return logo;
  }
}

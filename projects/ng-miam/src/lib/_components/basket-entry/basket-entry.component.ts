import { Component, OnInit, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { BasketEntry } from '../../_models/basket-entry';
import { Icon } from '../../_types/icon.enum';
import { Observable } from 'rxjs';

const MOBILE_BREAKPOINT = 768;

@Component({
  selector: 'ng-miam-basket-entry',
  templateUrl: './basket-entry.component.html',
  styleUrls: ['./basket-entry.component.scss']
})
export class BasketEntryComponent implements OnInit {
  mobile = window.innerWidth <= MOBILE_BREAKPOINT;
  showReplaceItem = false;
  public icon = Icon;
  
  @Input() entry: BasketEntry;
  @Input() tag?: string;
 
  @Output() entryRemoved = new EventEmitter<BasketEntry>();

  constructor() { }

  ngOnInit() {
    if (!this.entry.selectedItem || !this.entry.totalPrice || !this.entry.selectedItem.attributes['unit-price']) {
      console.error('Malformated basket entry', this.entry);
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.mobile = window.innerWidth <= MOBILE_BREAKPOINT;
  }

  save(): Observable<object> {
    this.entry.updateTotalPrice();
    return this.entry.save();
  }

  updateQuantity(increment: number) {
    if (this.entry.attributes.quantity + increment < 1) {
      this.entryRemoved.emit(this.entry);
    } else {
      this.entry.attributes.quantity += increment;
      this.save();
    }
  }

  toggleReplaceItem(show: boolean) {
    document.body.style.overflow = show ? 'hidden' : 'auto';
    this.showReplaceItem = show;
  }

}

import { ChangeDetectorRef, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { Ingredient } from '../../_models';
import { Recipe } from '../../_models/recipe';
import { RecipesService, GroceriesListsService, PointOfSalesService, ContextService, RecipeEventsService } from '../../_services';
import { Skeleton } from '../../_types/skeleton.enum';
import { Icon } from '../../_types/icon.enum';


@Component({ template: '' })
export abstract class AbstractRecipeCardComponent implements OnDestroy {
  @Input() headerText = '';
  @Output() hide: EventEmitter<void>;

  recipe: Recipe;
  hovered = false;
  ingredientsConcatName: string;
  icon = Icon;
  skeleton = Skeleton;

  protected subscriptions: Subscription[];

  constructor(
    public cdr: ChangeDetectorRef,
    public recipeService: RecipesService,
    public recipeEventsService: RecipeEventsService,
    public groceriesListsService: GroceriesListsService,
    protected posService: PointOfSalesService,
    private contextService: ContextService) {
    this.subscriptions = [];
    this.hide = new EventEmitter();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  addRecipe(display = true): void {
    this.posService.isPosValid().subscribe(isV => {
      if (isV) {
        if (!this.groceriesListsService.list$.value.hasRecipe(this.recipe.id)){
          this.recipeEventsService.sendEvent(this.recipe, this.recipeEventsService.ACTION_ADDED)
        }
        this.groceriesListsService.appendRecipeToList(this.recipe.id, this.recipe.modifiedGuests);
        if (display) {
          this.recipeService.displayObject(this.recipe, +this.recipe.modifiedGuests, true);
        }
      } else if (this.contextService.invalidPosCallback) {
        localStorage.setItem('_miam/cached-recipe', JSON.stringify(this.recipe));
        this.contextService.invalidPosCallback();
      }
    });
  }

  toggleHelper() {
    this.recipeService.toggleHelper();
    this.cdr.detectChanges();
  }

  toggleLike(event: Event) {
    this.preventClickPropagation(event);
    // TODO implement
  }

  openCalendar(event: Event) {
    this.preventClickPropagation(event);
    // TODO implement
  }

  shareRecipe(event: Event) {
    this.preventClickPropagation(event);
    // TODO implement
  }

  private preventClickPropagation(event: Event) {
    event.preventDefault();
    event.stopPropagation();
  }

  protected initIngredientsString(): void {
    this.ingredientsConcatName = '';
    if (this?.recipe?.relationships) {
      this.recipe.relationships[`ingredients`].data.forEach((ingredient: Ingredient, i: number) => {
        this.ingredientsConcatName += '' + ingredient.attributes.name;
        this.ingredientsConcatName += (i + 1 === this.recipe.relationships[`ingredients`].data.length) ? '' : ', ';
      });
    }
  }
}

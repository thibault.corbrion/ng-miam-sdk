import { ChangeDetectorRef, Component, EventEmitter, Input, Output, QueryList, ViewChildren } from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Icon } from '../../_types/icon.enum';
import * as _ from 'lodash';
import { take } from 'rxjs/operators';
import { IngredientsService } from '../../_services';
import { Ingredient } from '../../_models/ingredient';

@Component({
  selector: 'ng-miam-list-input',
  templateUrl: './list-input.component.html',
  styleUrls: ['./list-input.component.scss'],
})
export class ListInputComponent {

  @Input() hint: string;
  @Input() ingredientMode: boolean;
  @Input() list: FormArray = new FormArray([]);

  @Output() deleteId: EventEmitter<string>

  @ViewChildren('autosize') autosizes: QueryList<CdkTextareaAutosize>;

  public icon = Icon;
  public hideErrors: boolean;
  public fillListInput: FormControl;
  public mousseDownOnButton: boolean;


  constructor(private ingredientService: IngredientsService, private fb: FormBuilder, private cdr: ChangeDetectorRef) {
    this.fillListInput = new FormControl('');
    this.deleteId = new EventEmitter();
  }

  public delete(index: number): void {
    if (this.list.controls[index][`controls`].id?.value) {
      this.deleteId.emit(this.list.controls[index][`controls`].id.value);
    }
    this.list.removeAt(index);
    this.list.markAsDirty();
  }

  public submit(): void {
    const values = this.fillListInput.value?.trimEnd().split('\n');

    if (values && values.length > 0) {
      if (this.ingredientMode) {
        this.ingredientService.formatIngredients(values).pipe(take(1)).subscribe((ingredients: Ingredient[]) => {
          ingredients.forEach(ing => {
            this.list.push(this.fb.group({
              id: new FormControl(ing.id !== '-1' ? ing.id : null),
              attributes: this.fb.group({
                quantity: new FormControl(ing.attributes.quantity, Validators.required),
                unit: new FormControl(ing.attributes.unit, Validators.required),
                name: new FormControl(ing.attributes.name, Validators.required)
              })
            }));
            this.list.markAsDirty();
          });
        })
      } else {
        values.forEach((element: string) => {
          if (element.trim()) {
            this.list.push(this.fb.group({
              attributes: this.fb.group({
                description: new FormControl(element)
              })
            }));
          }
        });
      }
      this.list.markAsDirty();
    }

    this.fillListInput.reset();
    this.mousseDownOnButton = false;
  }

  public checkRow(index: number): void {
    if (!this.list.controls[index].get('attributes').get('description').value || !this.list.controls[index].get('attributes').get('description').value.trim()) {
      this.delete(index);
    }
  }

  public drag() {
    this.hideErrors = true;
  }

  public drop(event: CdkDragDrop<string[]>): void {
    moveItemInArray(this.list.controls, event.previousIndex, event.currentIndex);
    this.list.markAsDirty();
    this.hideErrors = false;
  }

  public checkIngredientdef(value: string, index: number) {
    this.ingredientService.isReviewedName(value).pipe(take(1)).subscribe(
      res => {
        _.assign(this.list.controls[index]["controls"]['attributes'], { isNotReviewedIngredientName: !res });
        this.cdr.detectChanges();
      }
    );
  }

  trackByIndex(index: number, el: any): string {
    return el;
  }
}

import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnChanges, OnInit, ViewEncapsulation } from '@angular/core';
import { combineLatest, Observable, of, Subscription } from 'rxjs';
import { skipWhile, switchMap, tap } from 'rxjs/operators';
import { BasketPreviewLine, PointOfSale, Recipe } from '../../_models';
import { RecipePricing } from '../../_models/recipe-pricing';
import { BasketsService, PointOfSalesService, RecipesService } from '../../_services';

@Component({
  selector: 'ng-miam-recipe-pricing',
  templateUrl: './recipe-pricing.component.html',
  styleUrls: ['./recipe-pricing.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RecipePricingComponent implements OnChanges, OnInit {
  @Input() recipe: Recipe;
  @Input() serves: number;
  pricing: RecipePricing;
  loading = true;
  isInBasket = false;

  private pos: PointOfSale;
  private subscriptions: Subscription[] = [];
  private onChangesSubscription: Subscription;

  constructor(
    private recipesService: RecipesService,
    private posService: PointOfSalesService,
    private cdr: ChangeDetectorRef,
    private basketsService: BasketsService
  ) { }

  ngOnInit() {
    this.subscriptions.push(
      this.initPricing().subscribe()
    );
  }

  ngOnChanges(): void {
    // Will execute everytime the recipe or the number of serves is changed
    // If the recipe is in basket, we ignore the change => pricing will get updated when the new basket preview is ready
    // Otherwise, we fetch the recipe pricing from the API
    if (!this.isInBasket) {
      this.loading = true;
      if (this.onChangesSubscription) {
        this.onChangesSubscription.unsubscribe();
      }
      this.onChangesSubscription = this.fetchPricing().subscribe();
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
    if (this.onChangesSubscription) {
      this.onChangesSubscription.unsubscribe();
    }
  }

  // Will execute every time the pos is changed or the basket preview is recalculated
  private initPricing(): Observable<RecipePricing> {
    return combineLatest([this.posService.pos$, this.basketsService.basketPreview$]).pipe(
      skipWhile(results => !results[0] || !results[1]),
      switchMap(results => {
        this.pos = results[0];
        const recipeLine = results[1].find(l => l.isRecipe && l.id?.toString() === this.recipe?.id?.toString());
        this.isInBasket = !!recipeLine;
        if (this.isInBasket) {
          // First, we check the basket preview : if it contains the recipe, we build the pricing from it
          return this.extractPricing(recipeLine);
        } else {
          // Otherwise, we fetch the recipe pricing from the API
          return this.fetchPricing();
        }
      })
    )
  }

  private fetchPricing(): Observable<RecipePricing> {
    if (!this.recipe || !this.pos || !this.serves) {
      return of(null);
    }
    return this.recipesService.getPricing(this.recipe.id, this.pos.id, this.serves).pipe(
      skipWhile(result => result.price === 0),
      tap((result: RecipePricing) => {
        this.pricing = result;
        this.loading = false;
        this.cdr.detectChanges();
      })
    )
  }

  private extractPricing(recipeLine: BasketPreviewLine): Observable<RecipePricing> {
    this.pricing = new RecipePricing({
      price: parseFloat(recipeLine.price),
      serves: recipeLine.count,
      price_per_serve: parseFloat(recipeLine.price) / recipeLine.count
    });
    this.loading = false;
    this.cdr.detectChanges();
    return of(this.pricing);
  }
}

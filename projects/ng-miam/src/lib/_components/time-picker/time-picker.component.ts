import { Component, Input, OnChanges } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Icon } from '../../_types/icon.enum';
import * as moment from 'moment';


@Component({
  selector: 'ng-miam-time-picker',
  templateUrl: './time-picker.component.html',
  styleUrls: ['./time-picker.component.scss']
})
export class TimePickerComponent implements OnChanges {

  @Input() icon: Icon;
  @Input() title: string;
  @Input() formControl: FormControl;

  private minMinuteRange = 0;
  private maxMinuteRange = 59;
  private hours: number;
  private minutes: number;

  public isHoursSelected: boolean;
  public isMinutesSelected: boolean;
  public internFormGroup: FormGroup;

  constructor() {
    this.internFormGroup = new FormGroup({
      hours: new FormControl(0),
      minutes: new FormControl(0),
    });
  }

  ngOnChanges(): void {
    if (this.formControl && this.formControl.value != null) {
      this.formatDuration(this.formControl.value);
      this.internFormGroup.patchValue({
        hours: this.hours,
        minutes: this.minutes
      });
    }
  }

  public minuteAutoCorrect(): void {
    this.isMinutesSelected = false;
    if (this.internFormGroup.value.minutes > this.maxMinuteRange) {
      this.internFormGroup.patchValue({
        hours: Math.floor(this.internFormGroup.value.minutes / 60) + +this.internFormGroup.value.hours,
        minutes: this.internFormGroup.value.minutes % 60
      });
    } else if (this.formControl.value < this.minMinuteRange) {
      this.formControl.patchValue(this.minMinuteRange);
    }
    this.durationChange();
  }

  public hoursBlur(): void {
    this.isHoursSelected = false;
    this.durationChange();
  }

  public durationChange() {
    if (this.valueIsInRange()) {
      const time = this.internFormGroup.getRawValue();
      const isoTime = moment.duration(time.hours, 'h').add(moment.duration(time.minutes, 'm')).toISOString();
      this.formControl.patchValue(isoTime);
    }
  }

  private valueIsInRange(): boolean {
    return this.formControl.value < this.maxMinuteRange || this.formControl.value > this.minMinuteRange;
  }

  private formatDuration(str: string): void {
    const duration = moment.duration(str);
    if (duration.asMinutes() >= 60) {
      this.hours = duration.hours();
      this.minutes = +duration.minutes().toFixed();
    } else {
      const d = duration.asMinutes();
      this.hours = 0;
      this.minutes = !d ? 0 : +d.toFixed();
    }
  }

}

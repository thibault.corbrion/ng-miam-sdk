import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Recipe } from '../../_models';



@Component({
  selector: 'ng-miam-addon-link',
  templateUrl: './addon-link.component.html',
  styleUrls: ['./addon-link.component.scss']
})
export class AddonLinkComponent {

  @Input() recipe: Recipe;
  @Output() showAddon: EventEmitter<void>;

  constructor() {
    this.showAddon = new EventEmitter();
  }

}

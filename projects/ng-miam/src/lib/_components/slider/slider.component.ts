import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Icon } from '../../_types/icon.enum';

@Component({
  selector: 'ng-miam-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnChanges {

  @Input() steps: string[];
  @Input() icons: Icon[];
  @Input() formControl: FormControl;

  @Output() valueChange: EventEmitter<string>;

  public stepsRange: number;
  
  constructor() {}

  ngOnChanges(): void {
    if (this.steps && this.steps.length > 0) {
      this.stepsRange = this.steps.length - 1;
    }
  }

  public getValue(): number {
    return this.steps.findIndex(el => el === this.formControl.value);
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BasketEntryComponent } from './basket-entry/basket-entry.component';
import { IconComponent } from './icon/icon.component';
import { CORSOverlayComponent } from './cors-overlay/cors-overlay.component';
import { ListInputComponent } from './list-input/list-input.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DragDropModule} from '@angular/cdk/drag-drop';
import { TextFieldModule } from '@angular/cdk/text-field';
import { SkeletonComponent } from './skeleton/skeleton.component';
import { SliderComponent } from './slider/slider.component';
import { TimePickerComponent } from './time-picker/time-picker.component';
import { CounterInputComponent } from './counter-input/counter-input.component';
import { TabsComponent } from './tabs/tabs.component';
import { TabBodyComponent } from './tabs/tab-body.component';
import { TabItemComponent } from './tabs/tab-item.component';
import { TabLabelComponent } from './tabs/tab-label.component';
import { DeleteConfirmButtonComponent } from './delete-confirm-button/delete-confirm-button.component';
import { RecipePricingComponent } from './recipe-pricing/recipe-pricing.component';
import { AddonLinkComponent } from './addon-link/addon-link.component';


export const COMPONENTS = [
  BasketEntryComponent,
  IconComponent,
  CORSOverlayComponent,
  ListInputComponent,
  SkeletonComponent,
  SliderComponent,
  TimePickerComponent,
  CounterInputComponent,
  TabsComponent,
  TabBodyComponent,
  TabItemComponent,
  TabLabelComponent,
  DeleteConfirmButtonComponent,
  RecipePricingComponent,
  AddonLinkComponent
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DragDropModule,
    TextFieldModule,
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS
})
export class ComponentsModule {
}



export interface MiamRecipe {
  'title': string;
  'number-of-guests': string;
  'preparation-time': string;
  'preheating-time'?: string;
  'cooking-time': string;
  'resting-time'?: string;
  'media-url': string;
  'ingredients-str': Array<string>;
  'ext-link'?: string;
  'source': string;
}

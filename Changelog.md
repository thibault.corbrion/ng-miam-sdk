## v2.19.0
- Refactor: move to userService some context function 
- New recipe card for catalog
- Added catalog page
- Load onScroll on catalog page
- Filters for the catalog
- New add on link on recipe detail
- rename recipe-detail-modal into recipe-modal
- Add print feature on recipe
- New recipe's label component
- Add a category component for the catalog
- Add a list component for the catalog
- Catalog page has a 'categories' mode and a 'list' mode
- Add sponsor logo on recipe card

## v2.18.2
- Fix recipe price when recipe already in basket (show price as per basket preview)

## v2.18.1
- Fix basketService.basketEntries() not returning basket entries any more

## v2.18.0
- Optimised basket loading process
- Add RecipesService.getPricing() & RecipePricing model
- Add RecipePricingComponent to display recipe card & recipe details
- Update recipe details design & add additional CTA at the bottom of the modal
- Add supplier webhook call method to SDK (and add token attribute to basket)
- send suggestion events

**breaking change on recipe details action & price css classes (fix naming & refactor)**

## v2.17.2
- unsuscribe from previous groceries_list calls to avoid race concurrencies

## v2.17.1
- change groceries list current route token

## v2.17.0
- Analytics service to be activated manually by client app
- Client app can forbid profiling when loading user context
- Add an event on recipe-card to know when it's hidden

## v2.16.9
- Fix total not being sent in cents

## v2.16.8
- Fix analytics and add logging on payment event

## v2.16.7
- force refresh current list request

## v2.16.6
- add few logs

## v2.16.5
- Fix analytics ID

## v2.16.4
- Fix analytics ID

## v2.16.3
- Fix analytics configuration

## v2.16.2
- Fix basket observable trigger after confirm
- Display required recipe after pos selection

## v2.16.1
- Fix _miam/listId kept in store while user logged in

## v2.16.0
- Add recipe selection log model and service
- Show recipes history related to logged in user
- Add pagination to recipes history
- Fix event propagation between basket preview line and delete button

**Depends on miam-api v3.48.0**

## v2.15.1
- Fix miam.user : add reset() & avoid double caching of user id or token

## v2.15.0
- Fix size of recipe helper popin
- Leverage backend current list mechanisms instead of creating/reseting lists in the front
- Fix infinite loading on basket details when adding a recipe that contains an entry in common with another added recipe

## v2.14.0
- Enable withCredentials: true
- Fix pipe Readable number for float between 0 and 1
- Fix countless units display
- Qté => Quantité

## v2.13.0
- Add new user identification method
- Fix pft products display

## v2.12.1
- Fix helper is now pushed in the DOM like details modal
- Fix recipe details modal not showing from basket preview line
- Fix recipe details modal not closing when invalid pos callback triggered

## v2.12.0
- Fix context service basket.stats$ behaviorsubject transformed to simple observable
- Add helper modal on recipe card
- Fix recipe price not updating when price = 0
- Adapt readable float number filter unit to plural / singular
- Avoid showing item replacement button if no candidate available
- Add supplier.load to contextService (and use this id to fetch suggestions)
- Allow client to define callback function if pos invalid
- Remove recipe active/reviewed attributes

## v2.11.0
- Add method to mass assign recipe shares
- Fix null basketStats on ContextService
- Avoid showing recipe card when null

## v2.10.1
- Fix missing import on basket entries service

## v2.10.0
- Add mediaMatcher from material CDN
- Load Pos from external id
- fix basket life cycle

## v2.9.0
- Fix styling
- Refactor recipe details modal & switch between details & basket preview
- Add confirm delete button
- Add Recipes history component

## v2.8.0
- Update styling on most components (recipe-card, recipe-details, counter-input, basket-preview, replace-item, modal...)
- Add guests mecanism (updating guests on recipe card reflects on recipe details and basket lines)
- Add transition between recipe details and basket preview
- Abstract suggestion mecanism from recipe card
- Icons can now be customized on lib initialization
- New classification for CSS classes throughout the lib, and update documentation accordingly

## v2.7.1
- Fix backward incompatibility

## v2.7.0
- Expose basket entries to context service
- Fix circular dependency on baskets
- Fix abstract component calling - ingredient_str on recipes without ingredients
- Fix propagation open recipe when using counter
- Fix recipe form

## v2.6.0
- Fix webcomponent in an angular application build
- Add angular-calendar dependency
- New feature calendar (component ,resource, service)
- New feature favorit list (component ,resource, service)
- Add an abstact level share between recipe-card and display card
- Fix circular dependency between groceries list service and context service

## v2.5.0
- Upgrade angular to 10
- Retrieve recipe partner from user info & set in context service
- Add recipe likes service

## v2.4.3
- Fix model index export list

## v2.4.2
- Fix web components  services  injections

## v2.4.1 
- fix ressource and service export and build

## v2.4.0
- Add recipe form component
- Add new API services

## v2.3.2
- Fix analytics tracking

## v2.3.1
- Fix analytics tracking by specifying a camp## v1.0.1

## v2.2.1
- Fix app initialization (remove initializer for recipe status / recipe provider)
- Adjust counter input styling (remove em / rem)
- Fix analytics events not including referrer

## v2.2.0
- Add token interceptor
- Add recipe suggestions

## v2.1.0
- Update documentation
- New view recipe detail
- Add CORS warning message

## v2.0.3
- Fix analytics

## v2.0.2
- Add analytics
- Append recipes
- CSS adjustments

## v2.0.1
- Flag moment as lib dependency

## v2.0.0
- Add webcomponents lib build & following components :
  - Basket preview
  - Counter
  - Drawer (with pos selection & basket preview)
  - Loader
  - Order button
  - Pos selection
  - Pos card
  - Recipe card
- Add services to interact with Miam API
- Improve Angular demo & add Webcomponents demo

## v1.2.1
- Use environment for paths
- Update package

## v1.2.0
- Fix build
- Update readme
- Remove active and suggested forbidden attributes
- Open basket on save
- Use ngx-jsonapi 2.1.12
- Externalize init factory

## v1.1.0
- Lock node version on gitlab
- Adjust button styling and use sessionStorage for list id
- Fix types in order component
- Fix list return & button animations
- Fix recipe check before creation
- Fix static @dynamic class anotation
- Add groceries list
- Fix removed recipe source in types

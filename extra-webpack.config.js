/**
 * Angular CLI use default name jsonpFunction when building an angular app.
 * So if there is two angular app at the same time they'll have the same name.
 * This name is use to lunch the app and then it can call two time one of
 * the application and never the other one.
 * To Solve this issue we use a custom webpack builder (@angular-builders/custom-webpack:browser)
 * because angular CLI doesn't allow us to change jsonpFunction's name
 * here we are overriding webpack conf
 */
module.exports = {
  output: {
  jsonpFunction: 'webpackJsonpWebcMiam',
  }
};